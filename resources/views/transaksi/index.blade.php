@extends('layouts.master')

@section('content')

<div class="container-fluid">
    <div class="row mb-2">
        <div class="col-sm-6">
            <h1>Data Transaksi</h1>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    @if(session('sukses'))
                    <div class="alert alert-success" role="alert">
                        {{session('sukses')}}
                    </div>
                    @endif

                    <!-- <h3 class="card-title">DataTable with default features</h3> -->
                    <div class="row">
                        <div class="col-6">
                            <h3>Total Transaksi: {{$count_transaksi ?? ''}}</h3>
                        </div>
                        <div class="col-6">
                            <a href="/transaksi/insert" class="btn btn-primary btn-sm float-right">Buat transaksi</a>
                        </div>
                    </div>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>ID No</th>
                                <th>Brand</th>
                                <th>OS</th>
                                <th>Processor</th>
                                <th>RAM</th>
                                <th>HDD</th>
                                <th>VGA</th>
                                <!-- <th>Aksi</th> -->
                            </tr>
                        </thead>
                        <tbody>
                            @if($data_transaksi ?? '' != '')
                            @foreach($data_transaksi ?? '' ?? '' as $transaksi)
                            <tr>
                                <td>{{$transaksi->id}}</td>
                                <td>{{$transaksi->idNo}}</td>
                                <td>{{$transaksi->brand}}</td>
                                <td>{{$transaksi->os}}</td>
                                <td>{{$transaksi->processor}}</td>
                                <td>{{$transaksi->ram}}</td>
                                <td>{{$transaksi->hdd}}</td>
                                <td>{{$transaksi->vga}}</td>
                                <!-- <td>
                                    <a href="/datatransaksi/{{$transaksi->kodetransaksi}}/edit" class="btn btn-warning btn-sm"><i class="fas fa-edit"></i></a>
                                    <a href="/datatransaksi/{{$transaksi->kodetransaksi}}/delete" class="btn btn-danger btn-sm" onclick="return confirm('Yakin dihapus?')"><i class="fas fa-trash"></i></a></td>
                                </td> -->
                            </tr>
                            @endforeach
                            @endif
                        </tbody>
                        <tfoot>
                            <tr>
                                <th>ID</th>
                                <th>ID No</th>
                                <th>Brand</th>
                                <th>OS</th>
                                <th>Processor</th>
                                <th>RAM</th>
                                <th>HDD</th>
                                <th>VGA</th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
                <!-- /.card-body -->
            </div>
            <!-- /.card -->


        </div>
    </div>
</div>

<!-- jQuery -->
<script src="/adminlte/plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="/adminlte/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- DataTables -->
<script src="/adminlte/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="/adminlte/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="/adminlte/plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="/adminlte/plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
<!-- AdminLTE App -->
<script src="/adminlte/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="/adminlte/js/demo.js"></script>

<!-- page script -->
<script>
    $(function() {
        $("#example1").DataTable({
            "responsive": true,
            "autoWidth": false,
        });
    });
</script>



@endsection