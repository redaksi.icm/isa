<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/login', 'User@login');
Route::get('/login', 'User@login');
Route::post('/loginPost', 'User@loginPost');
Route::get('/logout', 'User@logout');

Route::get('/dashboard', 'User@index');
Route::get('/test', 'User@test');

Route::get('/datauser', 'DaftarUser@index');
Route::post('/datauser/create', 'DaftarUser@create');

Route::get('/computer', 'ComputerInfo@index');
Route::post('/computer/create', 'ComputerInfo@create');

Route::get('/brand', 'Brand@index');
Route::post('/brand/create', 'Brand@create');

Route::get('/category', 'Category@index');
Route::post('/category/create', 'Category@create');

Route::get('/os', 'Os@index');
Route::post('/os/create', 'Os@create');

Route::get('/processor', 'Processor@index');
Route::post('/processor/create', 'Processor@create');

Route::get('/ram', 'Ram@index');
Route::post('/ram/create', 'Ram@create');

Route::get('/hdd', 'Hdd@index');
Route::post('/hdd/create', 'Hdd@create');

Route::get('/optical-drive', 'OpticalDrive@index');
Route::post('/optical-drive/create', 'OpticalDrive@create');

Route::get('/ethernet', 'Ethernet@index');
Route::post('/ethernet/create', 'Ethernet@create');

Route::get('/modem', 'Modem@index');
Route::post('/modem/create', 'Modem@create');

Route::get('/transaksi', 'Transaksi@index');
Route::get('/transaksi/insert', 'Transaksi@insert');
Route::post('/transaksi/create', 'Transaksi@create');

Route::get('/lap-transaksi', 'LaporanTransaksi@index');
Route::get('/lap-transaksi/pdf', 'LaporanTransaksi@pdf');
Route::get('/lap-transaksi/excel', 'LaporanTransaksi@excel');

Route::get('/lap-komputer', 'LaporanKomputer@index');
Route::get('/lap-komputer/pdf', 'LaporanKomputer@pdf');
Route::get('/lap-komputer/excel', 'LaporanKomputer@excel');
