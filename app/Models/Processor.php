<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Processor extends Model
{
    protected  $table = "processor";
    public $timestamp = false;

    protected $fillable = [
        'processor'
    ];
}
