<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ComputerInfo extends Model
{
    protected  $table = "computer_info";
}
