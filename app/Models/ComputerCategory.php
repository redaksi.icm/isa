<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ComputerCategory extends Model
{
    protected  $table = "computer_category";
    public $timestamp = false;

    protected $fillable = [
        'code',
        'category'
    ];
}
