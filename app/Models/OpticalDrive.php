<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OpticalDrive extends Model
{
    protected  $table = "optical_drive";
    public $timestamp = false;

    protected $fillable = [
        'optical_drive'
    ];
}
