<?php

namespace App\Http\Controllers;

use App\Models\Processor as ModelsProcessor;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class Processor extends Controller
{
    public function index()
    {
        if (!Session::get('login')) {
            return redirect('login')->with('alert', 'Kamu harus login dulu');
        } else {
            $data_processor = ModelsProcessor::all();
            $count_processor = $data_processor->count();

            return view(
                'processor.index',
                [
                    'data_processor' => $data_processor,
                    'count_processor' => $count_processor,
                    // 'data_channel' => $data_channel
                ]
            );
        }
    }

    public function create(Request $request)
    {
        ModelsProcessor::create($request->all());
        return redirect('/processor')->with('sukses', 'Data berhasil disimpan');
    }
}
