<?php

namespace App\Http\Controllers;

use App\Models\ComputerBrand;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class Brand extends Controller
{
    public function index()
    {
        if (!Session::get('login')) {
            return redirect('login')->with('alert', 'Kamu harus login dulu');
        } else {
            $data_brand = ComputerBrand::all();
            $count_brand = $data_brand->count();

            return view(
                'brand.index',
                [
                    'data_brand' => $data_brand,
                    'count_brand' => $count_brand
                ]
            );
        }
    }

    public function create(Request $request)
    {
        ComputerBrand::create($request->all());
        return redirect('/brand')->with('sukses', 'Data berhasil disimpan');
    }
}
