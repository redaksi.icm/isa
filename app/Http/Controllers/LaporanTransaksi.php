<?php

namespace App\Http\Controllers;

use App\Exports\TransaksiExport;
use App\Models\Transaksi;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\View;
use Maatwebsite\Excel\Facades\Excel;

class LaporanTransaksi extends Controller
{
    public function index()
    {
        if (!Session::get('login')) {
            return redirect('login')->with('alert', 'Kamu harus login dulu');
        } else {

            return view(
                'lap-transaksi.index'
            );
        }
    }

    public function pdf()
    {
        if (!Session::get('login')) {
            return redirect('login')->with('alert', 'Kamu harus login dulu');
        } else {
            $data_transaksi = Transaksi::all();

            // $view = (string)View::make('lap-transaksi.transaksi-pdf', ['data_transaksi' => $data_transaksi]);
            // return PDF::loadHTML($view)->setPaper('f4', 'landscape')->setWarnings(false)->save('myfile.pdf');

            $pdf = PDF::loadview('lap-transaksi.transaksi-pdf', ['data_transaksi' => $data_transaksi])->setPaper('f4', 'landscape')->setWarnings(false);
            return $pdf->download('transaksi-pdf.pdf');
            // return view('lap-transaksi.transaksi-pdf', ['data_transaksi' => $data_transaksi]);
        }
    }

    public function excel()
    {
        if (!Session::get('login')) {
            return redirect('login')->with('alert', 'Kamu harus login dulu');
        } else {
            return Excel::download(new TransaksiExport, 'transaksi.xlsx');
            // return "excel";
        }
    }
}
