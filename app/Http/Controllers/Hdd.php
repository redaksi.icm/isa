<?php

namespace App\Http\Controllers;

use App\Models\Hdd as ModelsHdd;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class Hdd extends Controller
{
    public function index()
    {
        if (!Session::get('login')) {
            return redirect('login')->with('alert', 'Kamu harus login dulu');
        } else {
            $data_hdd = ModelsHdd::all();
            $count_hdd = $data_hdd->count();

            return view(
                'hdd.index',
                [
                    'data_hdd' => $data_hdd,
                    'count_hdd' => $count_hdd,
                    // 'data_channel' => $data_channel
                ]
            );
        }
    }

    public function create(Request $request)
    {
        ModelsHdd::create($request->all());
        return redirect('/hdd')->with('sukses', 'Data berhasil disimpan');
    }
}
