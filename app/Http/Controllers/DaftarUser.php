<?php

namespace App\Http\Controllers;

use App\Models\DaftarUser as ModelsDaftarUser;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class DaftarUser extends Controller
{
    public function index()
    {
        if (!Session::get('login')) {
            return redirect('login')->with('alert', 'Kamu harus login dulu');
        } else {
            $data_user = ModelsDaftarUser::all();
            $count_user = $data_user->count();

            return view(
                'daftarUser.index',
                [
                    'data_user' => $data_user,
                    'count_user' => $count_user,
                    // 'data_channel' => $data_channel
                ]
            );
        }
    }

    public function create(Request $request)
    {
        ModelsDaftarUser::create($request->all());
        return redirect('/datauser')->with('sukses', 'Data berhasil disimpan');
    }
}
