<?php

namespace App\Http\Controllers;

use App\Models\ComputerInfo as ModelsComputerInfo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class ComputerInfo extends Controller
{
    public function index()
    {
        if (!Session::get('login')) {
            return redirect('login')->with('alert', 'Kamu harus login dulu');
        } else {
            $data_computer = ModelsComputerInfo::all();
            $count_computer = $data_computer->count();

            return view(
                'computer.index',
                [
                    'data_computer' => $data_computer,
                    'count_computer' => $count_computer,
                    // 'data_channel' => $data_channel
                ]
            );
        }
    }

    public function create(Request $request)
    {
        ModelsComputerInfo::create($request->all());
        return redirect('/computer')->with('sukses', 'Data berhasil disimpan');
    }
}
