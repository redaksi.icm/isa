<?php

namespace App\Http\Controllers;

use App\Models\Ethernet as ModelsEthernet;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class Ethernet extends Controller
{
    public function index()
    {
        if (!Session::get('login')) {
            return redirect('login')->with('alert', 'Kamu harus login dulu');
        } else {
            $data_ethernet = ModelsEthernet::all();
            $count_ethernet = $data_ethernet->count();

            return view(
                'ethernet.index',
                [
                    'data_ethernet' => $data_ethernet,
                    'count_ethernet' => $count_ethernet,
                    // 'data_channel' => $data_channel
                ]
            );
        }
    }

    public function create(Request $request)
    {
        ModelsEthernet::create($request->all());
        return redirect('/ethernet')->with('sukses', 'Data berhasil disimpan');
    }
}
