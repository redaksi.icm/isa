<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateComputerInfoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * 
     * @return void
     */
    public function up()
    {
        Schema::create('computer_info', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('idNo');
            $table->string('userId');
            $table->string('category');
            $table->string('brand');
            $table->string('type');
            $table->string('operatingSystem');
            $table->string('OSProductKey');
            $table->string('model');
            $table->string('serialNo');
            $table->string('processor');
            $table->string('speed');
            $table->string('ram');
            $table->string('hdd');
            $table->string('opticalDrive');
            $table->string('ethernet');
            $table->boolean('lan');
            $table->boolean('modem');
            $table->string('modemBrand');
            $table->boolean('screenFilter');
            $table->boolean('keyboard');
            $table->string('keyboardBrand');
            $table->string('keyboardType');
            $table->string('keyboardConnectivity');
            $table->boolean('mouse');
            $table->string('mouseBrand');
            $table->string('mouseType');
            $table->string('mouseConnection');
            $table->dateTime('purchaseDate');
            $table->string('supplierCode');
            $table->string('supplierName');
            $table->float('guarantee');
            $table->dateTime('guarenteeEndDate');
            $table->string('remark');
            $table->string('inputBy');
            $table->dateTime('lastUpdate');
            $table->boolean('del');
            $table->dateTime('activeDate');
            $table->boolean('fdd');
            $table->string('poNo');
            $table->float('price');
            $table->string('curr');
            $table->boolean('wifi');
            $table->string('wifiDesc');
            $table->string('idno1');
            $table->string('inno2');
            $table->boolean('backUp');
            $table->string('keteranganBackUp');
            $table->boolean('vgaShare');
            $table->string('vgaSize');
            $table->string('weight');
            $table->string('lanMacAdd');
            $table->boolean('online');
            $table->boolean('dipinjam');
            $table->string('keteranganPinjam');
            $table->boolean('underRepair');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('computer_info');
    }
}
